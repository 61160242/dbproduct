/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject;

/**
 *
 * @author gunm2
 */
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gunm2
 */
public class TestDeleteProduct {

    public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/Coffee.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "DELETE FROM product WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 9);
            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelecProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        
    }
}
