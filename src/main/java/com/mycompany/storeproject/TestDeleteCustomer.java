/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject;
import database.Database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gunm2
 */
public class TestDeleteCustomer {
     public static void main(String[] args) {
        Connection conn = null;
        String dbPath = "./db/store.db";
        Database db = Database.getInstance();
        conn = db.getConnection();
        
        //insert
        try {
            String sql = "DELETE FROM customer WHERE id = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            int row = stmt.executeUpdate();
            System.out.println("Affect row "+row);
        } catch (SQLException ex) {
            Logger.getLogger(TestInsertProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        db.close();
        
    }
    
}

